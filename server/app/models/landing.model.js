/*
    Author: Thanigaiselvan senthil shanmugam
*/
var sql = require('../../db.js');
var products='';

var LandingProduct = function(landing)
{
    this.productID=landing.productID;
    this.Product=landing.Product;
    this.company=landing.company;
    this.productImg=landing.productImg;
    this.TypeName=landing.TypeName;
}

//Retrieve gaming laptops to display in the landing page
LandingProduct.getGamingProducts = function (result)
{

        sql.query("select  distinct(product.productID),product.company,product.product,product.productImg from product inner join inventory on product.productID = inventory.productID where product.TypeName='Gaming' and product.isFeatured=1 and product.isDeleted=0 and inventory.availabilty =1  and (inventory.toDate is null or inventory.toDate='') LIMIT 5;", function (err, response) {
            if(err) {
                result(err, null);
            }
            else{
                //console.log(response);
                result(null, response);
          
            }
    
        });
}
//Retrieve  laptops for student to display in the landing page
LandingProduct.getStudentProducts = function (result)
{
    sql.query("select  distinct(product.productID),product.company,product.product,product.productImg from product inner join inventory on product.productID = inventory.productID where product.TypeName='Notebook' and product.isFeatured=1 and product.isDeleted=0 and inventory.availabilty =1 and (inventory.toDate is null or inventory.toDate='') LIMIT 5;", function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            //console.log(res);
            result(null, res);
      
        }

    });
}
//Retrieve laptops for business to display in the landing page
LandingProduct.getBusinessProducts = function (result)
{
    sql.query("select  distinct(product.productID),product.company,product.product,product.productImg from product inner join inventory on product.productID = inventory.productID where product.TypeName in ('Ultrabook','Workstation') and product.isFeatured=1 and product.isDeleted=0 and inventory.availabilty =1  and (inventory.toDate is null or inventory.toDate='') LIMIT 5;", function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
           result(null, res);
      
        }

    });
}

module.exports= LandingProduct;