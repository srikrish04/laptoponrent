'user strict';

var mysql = require('mysql');

//local mysql db connection
var connection = mysql.createConnection({
    host     : '54.162.42.117',
    user     : 'myuser',
    password : 'mypass',
    database : 'laptopOnRent',
    multipleStatements: true
});

connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection;