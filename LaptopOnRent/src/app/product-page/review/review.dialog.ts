/*
  Author: Thanigaiselvan Senthil Shanmugam
*/

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Component, Inject,Input} from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ReviewService } from 'src/app/services/reviewService';
export interface DialogData {
    starCount: number;
    review: string;
    ProductCount: number;
    
  }
@Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'review.dialog.html',
  })
  export class ReviewDialog {
    @Input('rating') rating: number=1;
    @Input('starCount') starCount: number = 5;
    @Input('color') color: string = "primary";
    review;
    productID
    reviewError=false;
    constructor(
      public dialogRef: MatDialogRef<ReviewDialog>,private _snackBar: MatSnackBar,
      @Inject(MAT_DIALOG_DATA) public data: DialogData,private reviewService:ReviewService) {
       // this.starCount=data.starCount;
       
       this.productID=data.ProductCount;
       
      }
  
    close(): void {
      this.dialogRef.close();
    }
    onRatingChanged(rating){
        console.log(rating);
        this.rating = rating;
      }
    
      //Method to submit the review
      submit(){
          let valid= true;
       
          console.log("reviewt"+this.review);
          if(this.review==undefined || this.review ==''){
              valid=false;
              this.reviewError=true;
          }
          if(valid){
            console.log("User name",sessionStorage.getItem('firstName') + ' '+ sessionStorage.getItem('secondName'))
            //review code
            this.reviewService.addReview({
              "uid" : sessionStorage.getItem('uid'),
              "productID": this.productID,
              "userName" : sessionStorage.getItem('firstName') + ' '+ sessionStorage.getItem('secondName'),
              "star":this.rating,
              "review": this.review,
              }).subscribe(data=>{
                console.log(data)
                if(data.success)
                {
                this._snackBar.open("Review added", "ok", {
                  duration: 2000,
                });
                this.dialogRef.close();
              }
                
            })
              //this.dialogRef.close();
          }

      }
}