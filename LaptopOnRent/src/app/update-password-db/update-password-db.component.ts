/*
Authors : Sreyas Naaraayanan,Hari Arunachalam 
*/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/authentication-service';
import { MatSnackBar } from '@angular/material';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-update-password-db',
  templateUrl: './update-password-db.component.html',
  styleUrls: ['./update-password-db.component.css']
})
export class UpdatePasswordDbComponent implements OnInit {
  email
  onInit;
  loginForm: FormGroup;
  constructor(private route: Router, private activateRoute: ActivatedRoute, private authService: AuthService, private _snackBar: MatSnackBar) {
    this.email = this.activateRoute.snapshot.paramMap.get("id");


    this.loginForm = new FormGroup({
      'username': new FormControl('', {}),
      'otp': new FormControl('', { validators: [Validators.required, , Validators.minLength(4), Validators.maxLength(4)] }),
      'password': new FormControl('', { validators: [Validators.required, , Validators.minLength(6), Validators.maxLength(20)] }),
      'confirmPassword': new FormControl('', { validators: [Validators.required, , Validators.minLength(6), Validators.maxLength(20)] })


    })

    this.loginForm.get('username').setValue(this.email);
  }

  ngOnInit() {

    this.onInit = false;
  }

  onPasswordReset() {
    if (this.loginForm.invalid) {
      this.onInit = true;

    }
    else {
      var pwd = this.loginForm.get('password').value;
      var confirmpwd = this.loginForm.get('confirmPassword').value;

      if (pwd == confirmpwd) {




        this.authService.updatePassword(this.loginForm.value).subscribe(data => {

          //sessionStorage.setItem('jwt', data.token);
          //this.loggedIn.emit("true")

          if (data.success) {

            this.route.navigate(["", "login"]);
          }

          else if (data.success == "wrongOTP") {
            this._snackBar.open(data.message, "Please re-enter your generated Password", {
              duration: 1500,
            });


            this.route.navigate(["", "updatePassword"]);

          }

          else {


            this._snackBar.open(data.message, "Please retry", {
              duration: 1500,
            });
          }
        })
      }
      else {
        this._snackBar.open("Passwords don't match", "OK", {
          duration: 1500,
        });

      }
    }


  }

}
