/*
  Authors: Srikrishna Sasidharan
*/
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PaymentService } from '../services/payment-service';
import { MatCardSmImage, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-choose-payment',
  templateUrl: './choose-payment.component.html',
  styleUrls: ['./choose-payment.component.css']
})
export class ChoosePaymentComponent implements OnInit {
   ELEMENT_DATA: Cards[] = [];
  securityPrice = 100.0
  displayedColumns: string[] = [ 'cardEnding', 'name', 'valid'];
  dataSource = this.ELEMENT_DATA;
  data: any;
  delivery;
  element;
  selectedPayment ;
  validateProceed = false;
  constructor(private _route:Router, private paymentService: PaymentService, public activatedRoute: ActivatedRoute,private _snackBar: MatSnackBar) {
    var uid = sessionStorage.getItem('uid');
    this.paymentService.getAllPayments({"uid": uid}).subscribe(data => {
      this.ELEMENT_DATA = []
      for(var i = 0; i<data.payments.length;i++){
       
       
        
        var expM = data.payments[i].expMonth
        var expY = data.payments[i].expYear
        var exp = expM +"/"+expY
        data.payments[i].cardNumber = "VISA ending with "+data.payments[i].cardNumber.slice( data.payments[i].cardNumber.length-4,)
        var obj :  Cards = {  'cardEnding': data.payments[i].cardNumber,'name': data.payments[i].name, 'valid': exp , 'id':data.payments[i].paymentid};
        
        
        this.ELEMENT_DATA.push(obj)
      }

       this.dataSource = this.ELEMENT_DATA
    })
  }

  ngOnInit() {
    this.delivery =sessionStorage.getItem('scheduledDelivery')
    this.data =  sessionStorage.getItem('price')
  }

  cardNumberControl = new FormControl('', [Validators.required]);
  cardNameControl = new FormControl('', [Validators.required]);
  expYearControl = new FormControl('', [Validators.required]);
  cvvControl = new FormControl('', [Validators.required]);

  expMonthControl = new FormControl('', [Validators.required]);
  selectFormControl = new FormControl('', Validators.required);
  expMonths: NewCardExpMonth[] = [
    {month:1, monthVal:1},
    {month:2, monthVal:2},
    {month:3, monthVal:3},
    {month:4, monthVal:4},
    {month:5, monthVal:5},
    {month:6, monthVal:6},
    {month:7, monthVal:7},
    {month:8, monthVal:8},
    {month:9, monthVal:9},
    {month:10, monthVal:10},
    {month:11, monthVal:11},
    {month:12, monthVal:12}

    
  ];
  expMonth;
  expYear;

  expYears: NewCardExpMonth[] = [
    {month:2020, monthVal:2020},
    {month:2021, monthVal:2021},
    {month:2022, monthVal:2022},
    {month:2023, monthVal:2023},
    {month:2024, monthVal:2024},
    {month:2025, monthVal:2025},
    {month:2026, monthVal:2026},
    {month:2027, monthVal:2027},
    {month:2028, monthVal:2028},
    {month:2029, monthVal:2029},
    {month:2030, monthVal:2030},
    {month:2031, monthVal:2031},
    {month:2032, monthVal:2032},
    {month:2033, monthVal:2033},
    {month:2034, monthVal:2034},

  ];

  //place order for user
  placeOrder(){
    var addrId = sessionStorage.getItem('selectedAddress')  
    var uid = sessionStorage.getItem('uid')

    var total = parseFloat(this.data) + this.securityPrice
    this.data =  sessionStorage.setItem('price',total.toString())
    if(this.selectedPayment!= undefined || this.selectedPayment != null){
      this.ELEMENT_DATA.forEach(data=>{
        if(this.selectedPayment == data['id']){
          sessionStorage.setItem('paymentFull', data['cardEnding'])
        }
      })
      this.validateProceed = false;
        this.paymentService.placeOrder({
          "uid" : uid,
          "totalAmount": total,
          "paymentid":this.selectedPayment,
          "paymentStatus":"S",
          "addressid":addrId
          }).subscribe(d=>{
          if(d.status){
            sessionStorage.setItem('orderid',d.orderid)
            this._route.navigate(["/order-confirmation"])
          }else{
            this._snackBar.open(d.message, "ok", {
              duration: 2000,
            });
          }
        });
      }else{
        this.validateProceed = true;
      }
  }

  //pay with new card and add user payment method
  payWithNewCard(){
 
    if(this.cardNumberControl.value!=undefined && this.cardNumberControl.value != null &&  this.cardNumberControl.value != ""
      && this.cardNameControl.value!=undefined && this.cardNameControl.value != null &&  this.cardNameControl.value != ""
      && this.expMonthControl.value!=undefined && this.expMonthControl.value != null &&  this.expMonthControl.value != ""
      && this.expYearControl.value!=undefined && this.expYearControl.value != null &&  this.expYearControl.value != ""
      
      && this.cvvControl.value!=undefined && this.cvvControl.value != null &&  this.cvvControl.value != "" ){

    
    var addrId = sessionStorage.getItem('selectedAddress')
    var uid = sessionStorage.getItem('uid')
    this.paymentService.addPayment({
      "uid": uid,
      "cardNumber" : this.cardNumberControl.value ,
      "name": this.cardNameControl.value,
      "expMonth" : this.expMonthControl.value.monthVal,
      "expYear" : this.expYearControl.value.monthVal,
      "type" : 'visa'
    }).subscribe(data=>{
      if(data.success){
        var total = parseFloat(this.data)
        this.paymentService.placeOrder({
          "uid" : uid,
          "totalAmount": total,
          "paymentid":1,
          "paymentStatus":"S",
          "addressid":addrId
          }).subscribe(d=>{
          if(d.status){
            sessionStorage.setItem('orderid',d.orderid)

            this._route.navigate(["/order-confirmation"])
          }else{
            this._snackBar.open(d.message, "ok", {
              duration: 2000,
            });
          }
        })
       
      }
    })
  }else{  

  }
  }

}
export interface Cards {
  cardEnding: string;
  name: string;
  valid: string;
  id : string;
}
export interface NewCardExpMonth {
  month: number;
  monthVal: number;
}