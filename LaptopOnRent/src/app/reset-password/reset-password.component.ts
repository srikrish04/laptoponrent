/*
  Authors: Sreyas Naaraayanan Ramanathan
*/
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/authentication-service';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  onInit;
  loginForm: FormGroup;

  constructor(private route: Router, private authService: AuthService, private _snackBar: MatSnackBar) {
    this.loginForm = new FormGroup({
      username: new FormControl('', { validators: [Validators.required, Validators.maxLength(30)] }),

    })


  }

  ngOnInit() {
    this.onInit = false;
  }

  onSendEmail() {

    if (this.loginForm.invalid) {
      this.onInit = true;

    }

    else {

      let body = {
        'username': this.loginForm.get('username').value
      }
      this.authService.sendEmail(body).subscribe(data => {


        if (data.success) {
          this.route.navigate(["", "updatePassword", this.loginForm.get('username').value]);
        }
        

        else {

          this._snackBar.open(data.message, "Sorry try again later", {
            duration: 1500,
          });
        }
      })




    }


  }

}
